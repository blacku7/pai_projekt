from flask import Flask, render_template, request
from flask_socketio import SocketIO, emit

app = Flask(__name__, static_url_path='/static')
socketio = SocketIO(app)

breeders_history = []
owners_history = []
dogbuy_history = []

dogs_sale_offers = []  

@app.route("/")
def home():
    return render_template("home.html")

@app.route("/breeders")
def breeders():
    image_filenames = ['pho1.jpg', 'pho2.jpg', 'pho3.jpg'] 
    return render_template("breeders.html", message_history=breeders_history, image_filenames=image_filenames)

@app.route("/owners")
def owners():
    image_filenames = ['pho2.jpg', 'pho3.jpg', 'pho1.jpg'] 
    return render_template("owners.html", message_history=owners_history, image_filenames=image_filenames)

@app.route("/dogbuy")
def dogbuy():
    image_filenames = ['pho2.jpg', 'pho3.jpg', 'pho1.jpg'] 
    return render_template("dogbuy.html", message_history=dogbuy_history, dogs_sale_offers = dogs_sale_offers, image_filenames=image_filenames)

@socketio.on("message")
def handle_message(msg):
    print('Wiadomość:', msg)
    if msg.get("chat") == "breeders":
        breeders_history.append(msg)
        emit("message", msg, broadcast=True)
    elif msg.get("chat") == "owners":
        owners_history.append(msg)
        emit("message", msg, broadcast=True)
    elif msg.get("chat") == "dogbuy":
        dogbuy_history.append(msg)
        emit("message", msg, broadcast=True)

@socketio.on("get_history")
def get_history(data):
    chat = data.get("chat")
    if chat == "breeders":
        emit("history", breeders_history)
    elif chat == "owners":
        emit("history", owners_history)
    elif chat == "dogbuy":
        emit("history", dogbuy_history)

@socketio.on("dog_sale_offer")
def handle_dog_sale_offer(offer):
    dogs_sale_offers.append(offer)
    emit("dog_sale_offer", offer, broadcast=True)

if __name__ == "__main__":
    socketio.run(app, debug=True)
